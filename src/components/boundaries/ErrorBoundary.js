import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errorSummary: "OoOoops! Algo deu errado...",
      error: null,
      errorInfo: null
    };
  }

  static propTypes = {
    errorSummary: PropTypes.string
  };

  componentDidCatch(error, errorInfo) {
    let errSum = this.props.errorSummary || this.state.errorSummary;
    this.setState({ errSum, error, errorInfo });
  }

  render() {
    if (this.state.errorInfo) {
      const { errorSummary, error, errorInfo } = this.state;
      return (
        <div className="section">
          <div className="container">
            <span className="hero title">{errorSummary}</span>
            <details style={{ whiteSpace: "pre-wrap" }}>
              {error && error.toString()}
              <br />
              {errorInfo.componentStack}
            </details>
            <div className="field">
              <Link to="/">Voltar ao início</Link>
            </div>
          </div>
        </div>
      );
    }
    return this.props.children;
  }
}

export default ErrorBoundary;
