import React from "react";
import PropTypes from "prop-types";

function CheckBox(props) {
  const { children, labelProps, ...rest } = props;
  return (
    <label {...labelProps}>
      <input type="checkbox" {...rest} />
      {children}
    </label>
  );
}

CheckBox.propTypes = {
  children: PropTypes.any.isRequired
};

export default CheckBox;
