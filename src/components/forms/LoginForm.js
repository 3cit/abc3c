import React from "react";
import EmailInput from "./EmailInput";
import PasswordInput from "./PasswordInput";
import Button from "./Button";
import Field from "./Field";
import Icon from "../layout/Icon";
import { connect } from "react-redux";
import {
  logUserIn,
  authenticateGoogleUser
} from "../../store/actions/authentication";
import GoogleAuthenticator from "./GoogleAuthenticator";
//import FacebookAuthenticator from "./FacebookAuthenticator";
import { Link, withRouter } from "react-router-dom";
//import Axios from "axios";

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: ""
    };
  }

  handleChange = evt => {
    this.setState({
      [evt.currentTarget.name]: evt.currentTarget.value
    });
  };

  handleSubmit = evt => {
    evt.preventDefault();
    const { username, password } = this.state;
    this.props.logUserIn({ username, password });
  };

  validateEmail = email => {
    if (!email) return email;
    // Testa a string digitada pelo usuário, caso exista, para verificar se bate
    // com um dos padrões utilizados, conforme mostrado abaixo:
    //  - mail@exemplo.com
    //  - Nome do Exemplo <mail@exemplo.com>
    // eslint-disable-next-line
    const pattern = /^"?([^\"\<\>\n]*)"?\ ?<?([\w\.\-\_]{3,}\@[\w\.\-]{2,}\.[\w]{2,})>?$/gim;
    return email.match(pattern) !== null;
  };

  validateUser = user => {
    if (!user) return;
    this.props.authenticateGoogleUser(user.token);
  };

  /* 
   * TODO: PEGAR IP do usuário no acesso e enviar ao 
     servidor nas operações POST/PUT/PATCH
  getMyIp = async () => {
    const res = await Axios.get("//api.ipify.org/?format=json");
    const { ip } = res.data;
    console.log(ip);
    return ip || null;
  };
  */

  render() {
    const usernameIsValid = this.validateEmail(this.state.username);
    return (
      <form onSubmit={this.handleSubmit}>
        <Field>
          <label htmlFor="id_username">Usuário (e-mail)</label>
          <EmailInput
            name="username"
            id="id_username"
            onChange={this.handleChange}
            value={this.state.username}
            isValid={usernameIsValid}
          />
        </Field>
        <Field>
          <label htmlFor="id_password">Senha</label>
          <PasswordInput
            name="password"
            id="id_password"
            onChange={this.handleChange}
            value={this.state.password}
          />
        </Field>
        <Field>
          <Button
            type="submit"
            className="is-primary is-fullwidth"
            disabled={
              !(
                this.state.username &&
                usernameIsValid &&
                this.state.password &&
                this.state.password.length > 6
              )
            }
          >
            <Icon icon="sign-in" />
            &nbsp; Entrar
          </Button>
        </Field>
        <Field>
          <GoogleAuthenticator callback={this.validateUser} />
        </Field>
        {/*<Field>
          <FacebookAuthenticator callback={userData => console.log(userData)} />
        </Field>*/}
        <div className="has-text-centered">
          ou ainda <Link to="/create-account/">criar uma conta</Link>.
        </div>
      </form>
    );
  }
}

export default withRouter(
  connect(
    null,
    { logUserIn, authenticateGoogleUser }
  )(LoginForm)
);
