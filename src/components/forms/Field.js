import React from "react";
import PropTypes from "prop-types";

function Field(props) {
  let className = "field";
  if (props.className) {
    className += " " + props.className;
  }
  return (
    <div className={className}>
      <div className={`control${props.isLoading ? " is-loading" : ""}`}>
        {props.children}
      </div>
    </div>
  );
}

Field.propTypes = {
  children: PropTypes.any.isRequired,
  isLoading: PropTypes.bool
};

export default Field;
