import React from "react";
import PropTypes from "prop-types";

const Button = ({ ...props }) => {
  const { type, className, children, ...rest } = props;
  return (
    <button type={type} className={`button ${className}`} {...rest}>
      {children}
    </button>
  );
};

Button.propTypes = {
  type: PropTypes.string.isRequired,
  children: PropTypes.any.isRequired
};

export default Button;
