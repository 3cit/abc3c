import React from "react";
import PropTypes from "prop-types";
import GoogleLogin from "react-google-login";
import Icon from "../layout/Icon";

function GoogleAuthenticator({ callback }) {
  function successCallback(response) {
    //userData
    const gUser = response.getBasicProfile();
    const name = gUser.getName().split(" ");
    const first_name = name[0];
    name.shift();
    const last_name = name.join(" ");
    const user = {
      id: gUser.getId(),
      first_name,
      last_name,
      email: gUser.getEmail(),
      token: response.tokenId
    };
    callback(user);
  }
  function failureCallback(response) {
    console.log("Failed.");
    //console.log(response);
    callback(null);
  }
  return (
    <GoogleLogin
      clientId={process.env.REACT_APP_GOOGLE_AUTH_CLIENT_ID}
      buttonText="Use sua conta Google"
      onSuccess={successCallback}
      onFailure={failureCallback}
      render={btnProps => (
        <button
          type="button"
          className="button is-fullwidth"
          onClick={btnProps.onClick}
          disabled={btnProps.disabled}
        >
          <Icon icon="google-plus" />
          <span>Usar sua conta Google</span>
        </button>
      )}
    />
  );
}

GoogleAuthenticator.propTypes = {
  callback: PropTypes.func.isRequired
};

export default GoogleAuthenticator;
