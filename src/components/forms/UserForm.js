import React from "react";
import Field from "./Field";
import EmailInput from "./EmailInput";
import TextInput from "./TextInput";
import PasswordInput from "./PasswordInput";
import Button from "./Button";
import CheckBox from "./CheckBox";
import Select from "./Select";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { loadingActivate } from "../../store/actions/global";
import {
  getPlans,
  getProfiles,
  alterUser,
  registerUser
} from "../../store/actions/authentication";
import { getPathToApi, getRequestHeader } from "../../store/actions/utils";
import Axios from "axios";

class UserForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      first_name: "",
      last_name: "",
      username: "",
      password: "",
      passwordConf: "",
      org_name: "",
      profile: "",
      plan: "",
      is_workgroup_admin: false,
      google_id: null,
      // others
      errors: {
        first_name: [],
        last_name: [],
        username: [],
        password: [],
        passwordConf: [],
        org_name: [],
        profile: [],
        plan: []
      }
    };
  }

  static propTypes = {
    availableProfiles: PropTypes.array.isRequired,
    availablePlans: PropTypes.array.isRequired,
    user: PropTypes.object, // usuário editor
    userInstance: PropTypes.object, // usuário editado
    isSelfUser: PropTypes.bool,
    getPlans: PropTypes.func.isRequired,
    getProfiles: PropTypes.func.isRequired,
    callback: PropTypes.func
  };

  componentDidMount() {
    if (this.props.userInstance) {
      if (this.props.user.is_workgroup_admin) this.props.getProfiles();
      this.mapPropsToLocalState();
    } else {
      this.props.getPlans();
    }
  }

  mapPropsToLocalState = () => {
    const {
      first_name,
      last_name,
      username,
      is_workgroup_admin,
      profile,
      workgroup,
      google_id
    } = this.props.userInstance;
    let org_name = "N/A";
    if (workgroup && workgroup.org_name) {
      org_name = workgroup.org_name;
    }
    this.setState({
      first_name,
      last_name,
      username,
      is_workgroup_admin,
      org_name,
      google_id,
      profile: profile && profile.id
    });
  };

  isAbleToSubmit = () => {
    const { first_name, username, org_name, plan } = this.state;
    if (
      first_name &&
      first_name.length &&
      username &&
      username.length &&
      org_name &&
      org_name.length
    ) {
      if (!this.props.userInstance) {
        for (let key in this.state.errors) {
          if (this.state.errors[key].length) {
            return false;
          }
        }
        if (plan && this.passwordsAreValid()) return true;
        else return false;
      } else {
        return true;
      }
    } else {
      return false;
    }
  };

  passwordsAreValid() {
    const { passwordConf, password } = this.state;
    return (
      password &&
      password.length &&
      passwordConf &&
      passwordConf.length &&
      password === passwordConf
    );
  }

  getWorkgroupFields = () => {
    const { org_name, is_workgroup_admin } = this.state;
    if (this.props.userInstance) {
      if (!this.props.user.is_workgroup_admin) {
        return null;
      }
      return (
        <>
          <Field>
            <label className="label" htmlFor="id_org_name">
              Organização
            </label>
            <p className="notification is-italic">{org_name}</p>
          </Field>
          <Field>
            <CheckBox
              labelProps={{ className: "is-size-6" }}
              type="checkbox"
              name="is_workgroup_admin"
              onChange={this.handleChange}
              checked={is_workgroup_admin}
            >
              Administrador
            </CheckBox>
          </Field>
        </>
      );
    } else {
      return (
        <Field>
          <label className="label" htmlFor="id_org_name">
            Organização
          </label>
          <TextInput
            id="id_org_name"
            name="org_name"
            onChange={this.handleChange}
            value={org_name}
            onBlur={this.verifyIfValueAlreadyExists}
            required
          ></TextInput>
          {this.getHelpText("org_name", "Nome da organização")}
        </Field>
      );
    }
  };

  getPasswordFields = () => {
    const { password, passwordConf } = this.state;
    if (this.props.userInstance && this.props.userInstance.id) {
      return (
        <>
          <span className="label">Alterar senha</span>
          <p>
            Para alterar sua senha, vá em <em>Usuário > Alterar senha</em>.
          </p>
        </>
      );
    } else {
      const passwordsOk = this.passwordsAreValid();
      return (
        <>
          <Field>
            <label className="label" htmlFor="id_password">
              Senha
            </label>
            <PasswordInput
              id="id_password"
              name="password"
              required
              value={password}
              onChange={this.handleChange}
            />
            <p className={`help${passwordsOk === false ? " is-danger" : ""}`}>
              {passwordsOk === false
                ? "Senhas não conferem."
                : "Sequência de pelo menos 8 dígitos usado para acesso ao sistema."}
            </p>
          </Field>
          <Field>
            <label className="label" htmlFor="id_password">
              Confirmação da senha
            </label>
            <PasswordInput
              id="id_password_conf"
              name="passwordConf"
              required
              value={passwordConf}
              onChange={this.handleChange}
            />
            <p className="help">Confirme a senha digitada acima.</p>
          </Field>
        </>
      );
    }
  };

  getProfileOrPlanField = () => {
    const { profile, plan } = this.state;
    const { isLoading } = this.props;
    const { availableProfiles, availablePlans } = this.props;
    if (this.props.userInstance && this.props.userInstance.id) {
      if (!availableProfiles || !availableProfiles.length) return null;
      return (
        <Field>
          <label className="label" htmlFor="id_profile">
            Perfil
          </label>
          <Select
            id="id_profile"
            name="profile"
            onChange={this.handleChange}
            isLoading={isLoading}
            value={profile}
            options={availableProfiles.map(v => ({
              value: v.id,
              text: v.name
            }))}
          />
        </Field>
      );
    } else {
      if (!availablePlans || !availablePlans.length) return null;
      return (
        <Field>
          <label className="label" htmlFor="id_plan">
            Plano
          </label>
          <Select
            id="id_plan"
            name="plan"
            onChange={this.handleChange}
            required
            value={plan}
            isLoading={isLoading}
            options={availablePlans.map(v => ({
              value: v.id,
              text: v.name
            }))}
          />
        </Field>
      );
    }
  };

  handleSubmit = evt => {
    evt.preventDefault();
    const {
      first_name,
      last_name,
      username,
      password,
      org_name,
      profile,
      plan,
      google_id
    } = this.state;
    const user = {
      first_name,
      last_name,
      username,
      org_name,
      plan,
      profile,
      google_id
    };
    if (this.props.userInstance && this.props.userInstance.id) {
      user.id = this.props.userInstance.id;
      delete user.org_name;
      if (
        this.props.userInstance.username.toLowerCase() ===
        username.toLowerCase()
      ) {
        delete user.username;
      }
      this.props.alterUser(user);
    } else {
      delete user.profile;
      if (this.props.user && this.props.user.is_workgroup_admin) {
        const workgroup = this.props.user.workgroup;
        user.org_name = workgroup.org_name;
        user.plan = workgroup.choosed_plan.id;
      }
      this.props.registerUser({ ...user, password });
      if (this.props.callback) {
        this.props.callback();
      }
    }
  };

  handleChange = evt => {
    let val = evt.currentTarget.value;
    if (evt.currentTarget.type === "checkbox") {
      val = evt.currentTarget.checked;
    }
    this.setState({ [evt.currentTarget.name]: val });
  };

  verifyIfValueAlreadyExists = evt => {
    let info = null;
    const { name, value } = evt.currentTarget;
    if (name === "org_name") {
      info = "workgroup";
    } else if (name === "username") {
      // email...
      info = "email";
    }
    if (info !== null && value && value.length) {
      const PATH = getPathToApi(`verify-${info}/?v=${value}`);
      const REQUEST_HEADER = getRequestHeader();
      this.props.loadingActivate(true);
      Axios.get(PATH, REQUEST_HEADER)
        .then(r => {
          if (r.data && r.data.result) {
            const { errors } = this.state;
            this.setState({
              //[name]: "",
              errors: {
                ...errors,
                [name]: [`Usuário com este ${info} já cadastrado.`]
              }
            });
          } else {
            const { errors } = this.state;
            this.setState({
              isLoading: false,
              errors: {
                ...errors,
                [name]: []
              }
            });
          }
          this.props.loadingActivate(false);
        })
        .catch(e => {
          console.log(e);
          this.setState({
            [evt.target.name]: ""
          });
          this.props.loadingActivate(false);
        });
    }
  };

  getHelpText = (field, defaultText) => {
    const { errors } = this.state;
    if (errors[field] && errors[field].length) {
      return <p className="help is-danger">{errors[field].join("; ")}</p>;
    } else {
      return <p className="help">{defaultText}</p>;
    }
  };

  render() {
    const { first_name, last_name, username } = this.state;
    const { isLoading } = this.props;
    return (
      <form onSubmit={this.handleSubmit}>
        <div className="columns">
          <div className="column">
            <div className="columns">
              <div className="column">
                <Field isLoading={isLoading}>
                  <label className="label" htmlFor="id_first_name">
                    Nome
                  </label>
                  <TextInput
                    id="id_first_name"
                    name="first_name"
                    required
                    onChange={this.handleChange}
                    value={first_name}
                  />
                </Field>
              </div>
              <div className="column">
                <Field isLoading={isLoading}>
                  <label className="label" htmlFor="id_last_name">
                    Sobrenome
                  </label>
                  <TextInput
                    id="id_last_name"
                    name="last_name"
                    onChange={this.handleChange}
                    value={last_name}
                  />
                </Field>
              </div>
            </div>
            <p className="help">Nome e sobrenome do usuário</p>
            <Field isLoading={isLoading}>
              <label className="label" htmlFor="id_username">
                E-mail
              </label>
              <EmailInput
                id="id_username"
                name="username"
                required
                onChange={this.handleChange}
                value={username}
                onBlur={this.verifyIfValueAlreadyExists}
              />
              {this.getHelpText("username", "E-mail utilizado para acesso")}
            </Field>
            {this.getPasswordFields()}
          </div>
          <div className="column">
            {this.getWorkgroupFields()}
            {this.getProfileOrPlanField()}
          </div>
        </div>
        <Field className="is-grouped is-grouped-centered">
          <Button
            type="submit"
            className={`is-primary${isLoading ? " is-loading" : ""}`}
            disabled={!this.isAbleToSubmit()}
          >
            Salvar
          </Button>
        </Field>
      </form>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLoading: state.global.isLoading,
    user: state.authentication.user,
    availableProfiles: state.authentication.profiles,
    availablePlans: state.authentication.plans
  };
}

const mapDispatchToProps = {
  getPlans,
  getProfiles,
  loadingActivate,
  alterUser,
  registerUser
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserForm);
