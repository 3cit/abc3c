import React from "react";
import PropTypes from "prop-types";

const EmailInput = ({ ...props }) => {
  const { className, isValid, ...rest } = props;
  return (
    <input
      type="email"
      className={`input ${className}${isValid === false ? " is-danger" : ""}`}
      {...rest}
    />
  );
};

EmailInput.propTypes = {
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  isValid: PropTypes.any
};

export default EmailInput;
