import React from "react";
import PropTypes from "prop-types";

function Select(props) {
  const { options, isLoading, disabled, ...restOfProps } = props;
  return (
    <div className={`select${isLoading ? " is-loading" : ""}`}>
      <select
        {...restOfProps}
        disabled={disabled === undefined ? options.length === 0 : disabled}
      >
        <option>Selecione um(a) opção</option>
        {options.map((opt, idx) => (
          <option
            key={`${props.name}_opt_${idx.toString()}`}
            value={opt.value.toString()}
          >
            {opt.text}
          </option>
        ))}
      </select>
    </div>
  );
}

Select.propTypes = {
  name: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.any.isRequired,
      text: PropTypes.string.isRequired
    })
  ),
  isLoading: PropTypes.bool
};

export default Select;
