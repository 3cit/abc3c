import React from "react";
import PropTypes from "prop-types";

const TextInput = ({ ...props }) => {
  const { className, ...rest } = props;
  return <input type="text" className={`input ${className}`} {...rest} />;
};

TextInput.propTypes = {
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired
};

export default TextInput;
