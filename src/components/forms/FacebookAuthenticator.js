import React from "react";
import PropTypes from "prop-types";
import Icon from "../layout/Icon";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";

function FacebookAuthenticator({ callback }) {
  function onSignIn(request) {
    callback(request);
  }
  return (
    <FacebookLogin
      appId={process.env.REACT_APP_FACEBOOK_AUTH_CLIENT_ID}
      fields="name,email,picture"
      callback={onSignIn}
      render={props => (
        <button
          type="button"
          className={`button is-fullwidth${
            props.isProcessing ? " is-loading" : ""
          }`}
          onClick={props.onClick}
          disabled={props.isDisabled}
        >
          <Icon icon="facebook" />
          <span>Usar sua conta Facebook</span>
        </button>
      )}
    />
  );
}

FacebookAuthenticator.propTypes = {
  callback: PropTypes.func.isRequired
};

export default FacebookAuthenticator;
