import React from "react";
import PropTypes from "prop-types";

const PasswordInput = ({ ...props }) => {
  const { className, ...rest } = props;
  return <input type="password" className={`input ${className}`} {...rest} />;
};

PasswordInput.propTypes = {
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired
};

export default PasswordInput;
