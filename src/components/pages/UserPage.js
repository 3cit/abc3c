import React from "react";
import PropTypes from "prop-types";
import { Switch, Route, withRouter } from "react-router-dom";
import UserResume from "./UserResume";
import UserEdit from "./UserEdit";
import UserActivities from "./UserActivities";

function UserPage({ match }) {
  return (
    <Switch>
      <Route path={`${match.path}/edit`} component={UserEdit} />
      <Route
        path={match.path}
        render={() => (
          <>
            <h2 className="title">Detalhes da conta</h2>
            <div className="columns">
              <div className="column">
                <UserResume />
              </div>
              <div className="column">
                <UserActivities />
              </div>
            </div>
            <div>Não há usuários adicionais para esta conta.</div>
          </>
        )}
      />
    </Switch>
  );
}

UserPage.propTypes = {
  match: PropTypes.object.isRequired
};

export default withRouter(UserPage);
