import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

const ProfessorPage = ({ match, history }) => {
  return (
    <>
      <h2 className="title">Página do professor</h2>
      <p>
        Aqui você terá todas as informações dos professores cadastrados na
        instituição de ensino, cadastrará novos e gerenciará todos eles.
      </p>
      <div className="field is-grouped is-grouped-centered">
        <p className="control">
          {/* eslint-disable-next-line */}
          <a onClick={history.goBack}>Voltar</a>
        </p>
      </div>
    </>
  );
};

ProfessorPage.propTypes = {
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
};

export default connect()(ProfessorPage);
