import React from "react";
import PropTypes from "prop-types";
//import ModalSkeleton from "../layout/modal-skeleton";
import UserForm from "../forms/UserForm";
import { connect } from "react-redux";
import GoogleAuthenticator from "../forms/GoogleAuthenticator";
import { alterUser } from "../../store/actions/authentication";
import Icon from "../layout/Icon";

function UserEdit({ user, alterUser }) {
  // Eliminar outros impecilhos que fazem este componente re-renderizar
  // 4x; adicionar uma notificação para informar que o sistema está
  // carregando algo.
  const onSignInGoogle = data => {
    // TODO: Realizar uma edição comum adicionando
    // o google ID no campo google_id do usuário
    alterUser({ google_id: data.id });
  };
  const removeGoogleAccount = () => {
    alterUser({ google_id: null });
  };
  return (
    <>
      <h2 className="title">Editar informações da conta</h2>
      <div className="columns">
        <div className="column is-3">
          {user.google_id ? (
            <div className="is-fullwidth">
              <Icon icon="check-circled" />
              Google &nbsp;
              <small>
                {/* eslint-disable-next-line */}[
                <a onClick={removeGoogleAccount}>remover</a>]
              </small>
            </div>
          ) : (
            <GoogleAuthenticator callback={onSignInGoogle} />
          )}
        </div>
        <div className="column">
          <UserForm userInstance={user} />
        </div>
      </div>
    </>
  );
}

UserEdit.propTypes = {
  user: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  return {
    user: state.authentication.user
  };
};

const mapDispatchToProps = {
  alterUser
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserEdit);
