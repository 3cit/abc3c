import React from "react";
import PropTypes from "prop-types";

const InstituitionPage = ({ match, history }) => {
  return (
    <>
      <h2 className="title">Página da instituição</h2>
      <p>Aqui você terá todas as informações da instituição de ensino.</p>
      <div className="field is-grouped is-grouped-centered">
        <p className="control">
          {/* eslint-disable-next-line */}
          <a onClick={history.goBack}>Voltar</a>
        </p>
      </div>
    </>
  );
};

InstituitionPage.propTypes = {
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
};

export default InstituitionPage;
