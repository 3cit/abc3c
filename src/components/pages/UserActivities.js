import React from "react";
import PropTypes from "prop-types";
import { getLastActivities } from "../../store/actions/authentication";
import { connect } from "react-redux";
import moment from "moment";

class UserActivities extends React.Component {
  static propTypes = {
    lastActivities: PropTypes.array.isRequired,
    getLastActivities: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.getLastActivities();
  }

  render() {
    return (
      <>
        <h2 className="subtitle">Últimas ações realizadas pelo usuário...</h2>
        <ul>
          {this.props.lastActivities.map(a => (
            <li key={`u_actv_${a.id}`}>
              {moment(a.instant).fromNow()} · {a.summary}
            </li>
          ))}
        </ul>
      </>
    );
  }
}

const mapStateToProps = state => {
  const a = state.authentication.lastActivities.slice(
    state.authentication.lastActivities.length - 5
  );
  return {
    lastActivities: a
  };
};

const mapDispatchToProps = {
  getLastActivities
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserActivities);
