import React from "react";

function Dashboard() {
  return (
    <>
      <h2 className="title">Dashboard</h2>
      <div className="columns">
        <div className="column">
          <div className="card">
            <div className="card-content">Gráficos financeiros</div>
          </div>
        </div>
        <div className="column">
          <div className="card">
            <div className="card-content">Gráficos alunos</div>
          </div>
          <div className="card">
            <div className="card-content">Gráficos previsões</div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Dashboard;
