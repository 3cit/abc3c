import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Link, withRouter } from "react-router-dom";
import Icon from "../layout/Icon";
import moment from "moment";

function UserResume({ match, user }) {
  function getUserLevel() {
    if (user.is_superuser) {
      return "Superusuário";
    } else if (user.is_staff) {
      return "Administrador";
    } else if (user.is_workgroup_admin) {
      return "Administrador do grupo de trabalho";
    } else {
      return "Comum";
    }
  }
  function getUserState() {
    if (user.is_active) {
      return "Ativo";
    } else {
      return <span className="has-text-danger">Inativo</span>;
    }
  }
  function getUserProfile() {
    if (user.profile) {
      return (
        <div>
          Perfil: {user.profile.name}
          <br />
          Permissões:
          <ul>
            {user.profile.permissions.map((perm, idx) => (
              <li key={`perm_${idx}`}>{perm}</li>
            ))}
          </ul>
        </div>
      );
    }
  }
  return (
    <div className="box">
      <span className="has-text-weight-semibold is-5">
        {user.first_name} {user.last_name}
      </span>
      <Link to={`${match.url}edit`} className="button is-small">
        <Icon icon="edit-alt" />
        <span>Editar</span>
      </Link>
      <br />
      Usuário: {user.username}
      <br />
      Trial limit: {(user.workgroup && user.workgroup.trial_limit) || "-"}
      <br />
      Nível: {getUserLevel(user)}
      <br />
      Criado em{" "}
      {moment(user.date_joined).format("DD [de] MMMM [de] Y[, às]HH:MM")}
      <br />
      {getUserState(user)}
      <br />
      {getUserProfile(user)}
    </div>
  );
}

UserResume.propTypes = {
  user: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  return {
    user: state.authentication.user
  };
};

export default withRouter(connect(mapStateToProps)(UserResume));
