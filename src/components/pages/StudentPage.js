import React from "react";
import PropTypes from "prop-types";

const StudentPage = ({ match, history }) => {
  return (
    <>
      <h2 className="title">Página dos alunos</h2>
      <p>
        Aqui você terá uma lista com todos os alunos da instituição, e poderá
        fazer o cadastro de novos estudantes. O acompanhamento estes estudantes
        também será realizado neste app.
      </p>
      <div className="field is-grouped is-grouped-centered">
        <p className="control">
          {/* eslint-disable-next-line */}
          <a onClick={history.goBack}>Voltar</a>
        </p>
      </div>
    </>
  );
};

StudentPage.propTypes = {
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
};

export default StudentPage;
