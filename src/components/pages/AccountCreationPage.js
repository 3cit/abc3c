import React from "react";
import UserForm from "../forms/UserForm";
import GoogleAuthenticator from "../forms/GoogleAuthenticator";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { getPathToApi, getRequestHeader } from "../../store/actions/utils";
import { showErrorMsg, showSuccessMsg } from "../../store/actions/messaging";
import { loadingActivate } from "../../store/actions/global";
import Axios from "axios";
import { withRouter } from "react-router-dom";

class AccountCreationPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      user: null
    };
  }

  static propTypes = {
    showSuccessMsg: PropTypes.func.isRequired,
    showErrorMsg: PropTypes.func.isRequired
  };

  accountAlreadyExists = async ({ email, google_id }) => {
    loadingActivate(true);
    const PATH = getPathToApi(`account-exists/?e=${email}&gid=${google_id}`);
    const response = await Axios.get(PATH, getRequestHeader());
    loadingActivate(false);
    if (response.data && response.data.result) {
      this.props.showErrorMsg(response.data.message);
      return true;
    } else {
      return false;
    }
  };

  socialLoginCallback = user => {
    if (
      !this.accountAlreadyExists({
        email: user.email,
        google_id: user.id
      })
    ) {
      this.props.showErrorMsg(
        "Quase lá, agora basta preencher o restante dos dados."
      );
      const { id, ...restOfUser } = this.user;
      this.setState({ user: { google_id: id, ...restOfUser } });
    }
  };

  render() {
    return (
      <div className="container">
        <section className="section">
          <h2 className="title">Criar conta</h2>
          <div className="columns">
            <div className="column is-3">
              <h4 className="subtitle is-5">Você também pode...</h4>
              <GoogleAuthenticator callback={this.socialLoginCallback} />
            </div>
            <div className="column">
              <p>
                Certifique-se de que os dados foram preenchidos corretamente
                antes de enviar formulário. Se preferir, você pode optar por se
                registrar por meio das redes sociais, através dos botões ao
                lado.
              </p>
              <UserForm
                userInstance={this.state.user}
                callback={() => this.props.history.goBack()}
              />
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default withRouter(
  connect(
    null,
    { showErrorMsg, showSuccessMsg }
  )(AccountCreationPage)
);
