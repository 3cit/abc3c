import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import propTypes from "prop-types";
import { connect } from "react-redux";
import LoginForm from "../forms/LoginForm";

class LandingPage extends Component {
  static propTypes = {
    isAuthenticated: propTypes.bool.isRequired
  };

  render() {
    if (this.props.isAuthenticated) {
      return <Redirect to="/" />;
    }

    return (
      <div
        className="login-page"
        style={{
          //backgroundImage:
          //  "url('https://images.theconversation.com/files/261625/original/file-20190301-110150-p6fmpr.jpg?ixlib=rb-1.1.0&q=45&auto=format&w=926&fit=clip')",
          //backgroundRepeat: "no-repeat",
          //backgroundPosition: "fixed",
          //backgroundSize: "100% auto",
          //backgroundOrigin: "content-box"
          backgroundColor: "#2a7221"
        }}
      >
        <div className="section">
          <div className="card login">
            <div className="card-content">
              <LoginForm />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.authentication.isAuthenticated
});

export default connect(mapStateToProps)(LandingPage);
