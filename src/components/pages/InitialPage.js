import React, { Component, Fragment } from "react";
import { Switch, Route, withRouter /*, Link*/ } from "react-router-dom";
import { connect } from "react-redux";
import PrivateRoute from "../security/PrivateRoute";
import PropTypes from "prop-types";
import Header from "../layout/Header";
import InstituitionPage from "./InstituitionPage";
import ProfessorPage from "./ProfessorPage";
import StudentPage from "./StudentPage";
import Dashboard from "./Dashboard";
import MainMenu from "../layout/MainMenu";
import UserPage from "./UserPage";
import Breadcrumb from "../layout/Breadcrumb";

export class InitialPage extends Component {
  static propTypes = {
    user: PropTypes.object.isRequired,
    isLoading: PropTypes.bool.isRequired,
    match: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  };

  render() {
    const { match } = this.props;
    return (
      <Fragment>
        <Header />
        <div className="columns is-fullheight">
          <div id="sidebar-menu" className="column section is-2">
            <MainMenu />
          </div>
          <div className="column">
            <div className="container">
              <section className="section">
                <Breadcrumb />
                <Switch>
                  <Route path={`${match.path}user`} component={UserPage} />
                  <Route
                    path={`${match.path}instituition`}
                    component={InstituitionPage}
                  />
                  <PrivateRoute
                    path={`${match.path}professors`}
                    component={ProfessorPage}
                  />
                  <Route
                    path={`${match.path}stutents`}
                    component={StudentPage}
                  />
                  <Dashboard />
                </Switch>
              </section>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

let mapStateToProps = state => ({
  user: state.authentication.user,
  isLoading: state.global.isLoading
});

export default withRouter(connect(mapStateToProps)(InitialPage));
