import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";

function MainMenu(props) {
  return (
    <aside className="menu">
      <p className="menu-label">Instituição</p>
      <ul className="menu-list">
        <li>
          {/* eslint-disable-next-line */}
          <a>Calendário de aulas</a>
        </li>
        <li>
          {/* eslint-disable-next-line */}
          <a>Alunos</a>
        </li>
      </ul>
      <p className="menu-label">Professores</p>
      <ul className="menu-list">
        <li>
          {/* eslint-disable-next-line */}
          <a>Team Settings</a>
        </li>
        <li>
          {/* eslint-disable-next-line */}
          <a className="is-active">Manage Your Team</a>
          <ul>
            <li>
              {/* eslint-disable-next-line */}
              <a>Members</a>
            </li>
            <li>
              {/* eslint-disable-next-line */}
              <a>Plugins</a>
            </li>
            <li>
              {/* eslint-disable-next-line */}
              <a>Add a member</a>
            </li>
          </ul>
        </li>
        <li>
          {/* eslint-disable-next-line */}
          <a>Invitations</a>
        </li>
        <li>
          {/* eslint-disable-next-line */}
          <a>Cloud Storage Environment Settings</a>
        </li>
        <li>
          {/* eslint-disable-next-line */}
          <a>Authentication</a>
        </li>
      </ul>
      <p className="menu-label">Financeiro</p>
      <ul className="menu-list">
        <li>
          {/* eslint-disable-next-line */}
          <a>Lançamentos</a>
        </li>
        <li>
          {/* eslint-disable-next-line */}
          <a>Balanço</a>
        </li>
      </ul>
    </aside>
  );
}

MainMenu.propTypes = {
  match: PropTypes.object.isRequired
};

export default withRouter(MainMenu);
