import React from "react";
import PropTypes from "prop-types";
import Icofont from "react-icofont";

const Icon = ({ icon, config }) => {
  const getExtraClasses = function() {
    if (!config) return "";
    let extraClasses = "";
    if (config.size) extraClasses += ` ${config.size}`;
    if (config.type) extraClasses += ` has-text-${config.type}`;
    return extraClasses;
  };
  return (
    <span className={`icon${getExtraClasses()}`}>
      <Icofont icon={icon} />
    </span>
  );
};

Icon.propTypes = {
  icon: PropTypes.string.isRequired,
  config: PropTypes.shape({
    size: PropTypes.string,
    type: PropTypes.oneOf(["success", "danger", "warning", "info"])
  })
};

export default Icon;
