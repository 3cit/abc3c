import React from "react";
import PropTypes from "prop-types";

const ModalSkeltal = props => {
  return (
    <div className={`modal${props.isModalActive ? " is-active" : ""}`}>
      <div className="modal-background" />
      <div className="modal-content">
        <div className="box">{props.children}</div>
      </div>
      <button
        className="modal-close is-large"
        aria-label="close"
        onClick={props.handleModalClose}
      />
    </div>
  );
};

ModalSkeltal.propTypes = {
  handleModalClose: PropTypes.func.isRequired,
  isModalActive: PropTypes.bool.isRequired
};

export default ModalSkeltal;
