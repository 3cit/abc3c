import React from "react";
import { NavLink, withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import Icon from "./Icon";

const PATHS = [
  {
    path: "user/",
    text: "Usuário",
    icon: "user"
  },
  {
    path: "user/edit",
    text: "Alteração",
    icon: "edit-alt"
  }
];
function Breadcrumb(props) {
  const { location, match } = props;
  function getPagePath() {
    const treeLocations = [];
    //console.log(location.pathname);
    PATHS.forEach(function(p, idx) {
      if (location.pathname && location.pathname.search(p.path) >= 0) {
        const path = `${match.url}${p.path}`;
        treeLocations.push(
          <li
            key={`breadcrumb_${idx.toString()}`}
            className={location.pathname === path ? "is-active" : null}
          >
            <NavLink to={path}>
              <Icon icon={p.icon} size="small" />
              <span>{p.text}</span>
            </NavLink>
          </li>
        );
      }
    });
    return treeLocations;
  }
  //console.log(props.match, props.location, props.history);
  return (
    <nav className="breadcrumb is-small" aria-label="breadcrumbs">
      <ul>
        <li>
          <NavLink to={`${match.url}`}>
            <Icon icon="home" size="small" />
            <span>Início</span>
          </NavLink>
        </li>
        {getPagePath()}
      </ul>
    </nav>
  );
}

Breadcrumb.propTypes = {
  match: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
};

export default withRouter(Breadcrumb);
