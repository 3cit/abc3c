import React from "react";
import { types } from "react-alert";
import Icon from "./Icon";

const getIcon = type => {
  switch (type) {
    case types.ERROR:
      return "exclamation";
    case types.SUCCESS:
      return "verification-check";
    case types.INFO:
    default:
      return "info";
  }
};

const AlertTemplate = ({ message, options, style, close }) => (
  <article
    className={`message is-${
      options.type === "error" ? "danger" : options.type
    }`}
    style={style}
  >
    <div className="message-body">
      <Icon icon={getIcon(options.type)} />
      <span className="is-small">{message}</span>
      <button className="delete" onClick={close} />
    </div>
  </article>
);

export default AlertTemplate;
