import React, { Component, Fragment } from "react";
import { NavLink, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";

import { logUserOut } from "../../store/actions/authentication";
import Icon from "./Icon";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isMenuActivate: false
    };
  }
  static propTypes = {
    user: PropTypes.object.isRequired,
    isAuthenticated: PropTypes.bool.isRequired,
    logUserOut: PropTypes.func.isRequired,
    location: PropTypes.object.isRequired
  };

  toggleMenu = e => {
    let burgerButtonElement = e.currentTarget;
    let navbarElement = e.currentTarget.parentElement.parentElement;
    let navbarMenuElement = navbarElement.getElementsByClassName("navbar-menu");
    if (navbarMenuElement) {
      navbarMenuElement = navbarMenuElement[0];
    }
    burgerButtonElement.classList.toggle("is-active");
    navbarMenuElement.classList.toggle("is-active");
  };

  componentDidMount() {
    document
      .getElementsByTagName("body")[0]
      .classList.add("has-navbar-fixed-top");
  }

  componentWillUnmount() {
    document
      .getElementsByTagName("body")[0]
      .classList.remove("has-navbar-fixed-top");
  }

  handleMenuActivate = () => {
    document.getElementById("sidebar-menu").classList.toggle("is-active");
    this.setState({ isMenuActivate: !this.state.isMenuActivate });
  };

  render() {
    // TODO: Estudar uma solução melhor para esse menu lateral, no quesito 'responsibilidade'
    return (
      <nav
        className="navbar is-primary is-fixed-top"
        role="navigation"
        aria-label="main navigation"
      >
        <div className="navbar-start">
          {/* eslint-disable-next-line */}
          <a
            className={`navbar-item${
              this.state.isMenuActivate ? " is-active" : ""
            }`}
            title={`Clique para ${
              this.state.isMenuActivate ? "ocultar" : "exibir"
            } o menu`}
            onClick={this.handleMenuActivate}
          >
            <Icon
              icon={this.state.isMenuActivate ? "arrow-left" : "arrow-right"}
            />
          </a>
        </div>
        <div className="navbar-brand">
          <div
            style={{
              backgroundImage: `url('http://3cit.com.br/img/logo3cit.png')`,
              backgroundOrigin: 0,
              backgroundRepeat: "no-repeat",
              backgroundSize: "100px 50px",
              width: "140px",
              height: "40px"
            }}
          />
          {/* eslint-disable-next-line */}
          <a
            role="button"
            className="navbar-burger burger"
            aria-label="menu"
            aria-expanded="false"
            data-target="mainNavbarHelp3cit"
            onClick={this.toggleMenu}
          >
            <span aria-hidden="true" />
            <span aria-hidden="true" />
            <span aria-hidden="true" />
          </a>
        </div>

        <div id="mainNavbarHelp3cit" className="navbar-menu">
          <div className="navbar-end">
            {this.props.isAuthenticated ? (
              <Fragment>
                <span className="navbar-item">Olá,</span>
                <NavLink
                  className="navbar-item"
                  activeClassName="is-active"
                  to={`${this.props.match.url}user/`}
                >
                  {(this.props.user && this.props.user.first_name) || (
                    <em>anônimo</em>
                  )}
                  &nbsp; ({this.props.user && this.props.user.username})
                </NavLink>
                {/* eslint-disable-next-line */}
                <a onClick={this.props.logUserOut} className="navbar-item">
                  Sair
                </a>
              </Fragment>
            ) : null}
          </div>
        </div>
      </nav>
    );
  }
}

const mapStateToProps = state => ({
  user: state.authentication.user,
  isAuthenticated: state.authentication.isAuthenticated
});

export default withRouter(
  connect(
    mapStateToProps,
    { logUserOut }
  )(Header)
);
