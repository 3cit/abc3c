import React from "react";

export default function Footer() {
  return (
    <footer
      className="is-fixed-bottom"
      style={{
        display: "block",
        marginTop: "-20px",
        marginLeft: "auto",
        marginRight: "auto",
        opacity: "0.60"
      }}
    >
      <a href="http://3cit.com.br/" title="Visite nosso site">
        Centro de Computação Criativa
      </a>
      , 2019
    </footer>
  );
}
