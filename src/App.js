import React, { Component } from "react";
import { createBrowserHistory } from "history";
import { Provider } from "react-redux";
import { Router, Route, Switch } from "react-router-dom";
import { transitions, positions, Provider as AlertProvider } from "react-alert";

import store from "./store";
import PrivateRoute from "./components/security/PrivateRoute";
import { authenticateUser } from "./store/actions/authentication";

//import logo from "./logo.svg";
import "./App.scss";
import Alert from "./components/other/Alert";
import AlertTemplate from "./components/layout/AlertTemplate";
import InitialPage from "./components/pages/InitialPage";
import LandingPage from "./components/pages/LandingPage";
import ErrorBoundary from "./components/boundaries/ErrorBoundary";
import AccountCreationPage from "./components/pages/AccountCreationPage";
import NotFoundPage from "./components/pages/NotFoundPage";
//import Footer from "./components/layout/Footer";

const alertOptions = {
  position: positions.TOP_RIGHT,
  timeout: 3000,
  transition: transitions.FADE,
  template: AlertTemplate
};

const browserHistory = createBrowserHistory({
  basename: "/"
});

class App extends Component {
  componentDidMount = () => {
    store.dispatch(authenticateUser());
  };

  render() {
    return (
      <Provider store={store}>
        <AlertProvider {...alertOptions}>
          <Router history={browserHistory}>
            <ErrorBoundary>
              <Alert />
              <Switch>
                <Route exact path="/login/" component={LandingPage} />
                <Route
                  exact
                  path="/create-account/"
                  component={AccountCreationPage}
                />
                <PrivateRoute path="/" component={InitialPage} />
                <Route component={NotFoundPage} />
              </Switch>
            </ErrorBoundary>
          </Router>
        </AlertProvider>
      </Provider>
    );
  }
}

export default App;
