import { combineReducers } from "redux";
import authentication from "./authentication";
import messaging from "./messaging";
import global from "./global";

export default combineReducers({
  messaging,
  authentication,
  global
});
