import {
  AUTHENTICATED_USER,
  AUTHENTICATION_FAILED,
  LOGGED_IN_USER,
  LOGGED_OUT_USER,
  ALTERED_USER,
  ALTERING_USER,
  USER_ALTER_FAILED,
  AUTHENTICATING_USER,
  GET_PROFILES_FAILED,
  GET_PLANS_FAILED,
  GET_PLANS,
  GET_PROFILES,
  ACCOUNT_CREATION_FAILED,
  GET_LAST_USER_ACTIVITIES,
  GET_LAST_USR_ACTV_FAILED
} from "../actions/_types";
import { getAccessToken, setAccessToken, removeAccessToken } from "./utils";

const initialState = {
  user: null,
  token: getAccessToken() || null,
  isAuthenticated: false,
  plans: [],
  profiles: [],
  errors: {},
  lastActivities: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case AUTHENTICATING_USER:
      return {
        ...state
        //isLoading: true
      };
    case AUTHENTICATED_USER:
      return {
        ...state,
        //isLoading: false,
        user: action.payload,
        isAuthenticated: true
      };
    case LOGGED_IN_USER:
      const { token, ...user } = action.payload;
      setAccessToken(token);
      return {
        ...state,
        //isLoading: false,
        token: token,
        user: user,
        isAuthenticated: true
      };
    case AUTHENTICATION_FAILED:
    case LOGGED_OUT_USER:
      removeAccessToken();
      return {
        ...state,
        token: null,
        user: null,
        isAuthenticated: false
      };
    case ALTERED_USER:
      return {
        ...state,
        user: action.payload
      };
    case GET_PROFILES:
      return {
        ...state,
        profiles: action.payload
      };
    case GET_PLANS:
      return {
        ...state,
        plans: action.payload
      };
    case ACCOUNT_CREATION_FAILED:
      return {
        ...state,
        errors: {
          ...state.errors,
          ...action.payload
        }
      };
    case GET_LAST_USER_ACTIVITIES:
      return {
        ...state,
        lastActivities: action.payload
      };
    case GET_LAST_USR_ACTV_FAILED:
    case GET_PLANS_FAILED:
    case GET_PROFILES_FAILED:
    case ALTERING_USER:
    case USER_ALTER_FAILED:
    default:
      return state;
  }
}
