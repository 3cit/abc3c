// Request setting header
export const REQUEST_HEADER = {
  headers: {
    "Content-Type": "application/json; charset=utf-8",
    Accept: "application/json"
  }
};

export const getPathToApi = path => {
  const {
    REACT_APP_API_PROTOCOL,
    REACT_APP_API_SERVER,
    REACT_APP_API_PREFIX
  } = process.env;
  return `${REACT_APP_API_PROTOCOL}://${REACT_APP_API_SERVER}/${REACT_APP_API_PREFIX}${path}`;
};

export const getRequestHeader = (token = null) => {
  if (Boolean(token)) {
    return {
      headers: {
        ...REQUEST_HEADER.headers,
        Authorization: "Token " + token
      }
    };
  }
  return REQUEST_HEADER;
};
