import { SET_LOADING, UNSET_LOADING } from "./_types";

export function loadingActivate(b) {
  return { type: b ? SET_LOADING : UNSET_LOADING };
}
