import Axios from "axios";
import {
  AUTHENTICATED_USER,
  AUTHENTICATION_FAILED,
  ACCOUNT_CREATION_FAILED,
  ALTERED_USER,
  USER_ALTER_FAILED,
  LOGGED_IN_USER,
  LOGGED_OUT_USER,
  SET_LOADING,
  UNSET_LOADING,
  GET_PLANS,
  GET_PLANS_FAILED,
  GET_PROFILES,
  GET_PROFILES_FAILED,
  GET_LAST_USER_ACTIVITIES
} from "./_types";
import { extractErrors, errorMessage, successMessage } from "./messaging";
import { getPathToApi, getRequestHeader, REQUEST_HEADER } from "./utils";

export const authenticateUser = () => (dispatch, getState) => {
  dispatch({
    type: SET_LOADING
  });
  const REQUEST_HEADER = getRequestHeader(getState().authentication.token);
  const PATH = getPathToApi("user/");
  Axios.get(PATH, REQUEST_HEADER)
    .then(r =>
      dispatch({
        type: AUTHENTICATED_USER,
        payload: r.data
      })
    )
    .catch(err => {
      console.error(err);
      dispatch({
        type: AUTHENTICATION_FAILED
      });
    })
    .finally(() => dispatch({ type: UNSET_LOADING }));
};

export const logUserIn = credentials => dispatch => {
  const { username, password } = credentials;
  const body = JSON.stringify({ username, password });
  const REQUEST_HEADER = getRequestHeader();
  const PATH = getPathToApi("login/");
  Axios.post(PATH, body, REQUEST_HEADER)
    .then(response => {
      dispatch({
        type: LOGGED_IN_USER,
        payload: response.data
      });
    })
    .catch(err => {
      if (err.response) {
        dispatch(extractErrors(err.response.data, err.response.status));
      } else {
        dispatch(
          errorMessage(
            "Falha ao verificar ação: Usuário não possui requisitos necessários para realizar a ação."
          )
        );
      }
      dispatch({
        type: AUTHENTICATION_FAILED
      });
    });
};

export const logUserOut = () => (dispatch, getState) => {
  dispatch({
    type: SET_LOADING
  });

  const REQUEST_HEADER = getRequestHeader(getState().authentication.token);
  const PATH = getPathToApi("revoke-token/");

  Axios.post(PATH, null, REQUEST_HEADER)
    .then(() => {
      dispatch(successMessage("Até logo!"));
      dispatch({
        type: LOGGED_OUT_USER
      });
    })
    .catch(() => dispatch(errorMessage("O usuário já se encontra deslogado.")))
    .finally(() => dispatch({ type: UNSET_LOADING }));
};

export const registerUser = ({
  first_name,
  last_name,
  username,
  password,
  org_name,
  plan,
  google_id
}) => (dispatch, getState) => {
  const token = getState().authentication.token;
  const REQUEST_HEADER = getRequestHeader(token ? token : null);
  const PATH_TO_REGISTER = getPathToApi("users/new/");
  const user = JSON.stringify({
    first_name,
    last_name,
    username,
    password,
    org_name,
    google_id,
    choosed_plan: plan
  });
  dispatch({ type: SET_LOADING });
  Axios.post(PATH_TO_REGISTER, user, REQUEST_HEADER)
    .then(() => {
      dispatch({
        type: UNSET_LOADING
      });
      if (!token)
        dispatch(successMessage(`Um e-mail foi enviado para ${username}`));
    })
    .catch(err => {
      let payload = {
        non_field_errors: ["Falha ao cadastrar usuário: erro desconhecido."]
      };
      if (err.response.data) {
        dispatch(extractErrors(err.response.data, err.response.status));
        payload = err.response.data;
      }
      dispatch({ type: ACCOUNT_CREATION_FAILED, payload });
    })
    .finally(() => dispatch({ type: UNSET_LOADING }));
};

export const alterUser = user => (dispatch, getState) => {
  let currentState = getState();
  const REQUEST_HEADER = getRequestHeader(currentState.authentication.token);
  const PATH = getPathToApi("user/");
  dispatch({
    type: SET_LOADING
  });

  // TODO: Verificar se o username foi alterado. Se sim, notificar o usuário
  // da confirmação do e-mail.
  Axios.patch(PATH, user, REQUEST_HEADER)
    .then(response => {
      dispatch(successMessage("Dados salvos"));
      dispatch({
        type: ALTERED_USER,
        payload: response.data
      });
      if (user.username) {
        dispatch(
          successMessage(
            "Um e-mail de confirmação foi enviado para " + user.username
          )
        );
      }
    })
    .catch(err => {
      console.log(err.response.data);
      if (err.response.data)
        dispatch(extractErrors(err.response.data, err.response.status));
      dispatch(
        errorMessage("Não foi possível atualizar usuário. Tente novamente.")
      );
      dispatch({ type: USER_ALTER_FAILED });
    })
    .finally(() => dispatch({ type: UNSET_LOADING }));
};

export const getLastActivities = () => (dispatch, getState) => {
  dispatch({ type: SET_LOADING });
  const PATH = getPathToApi("user-activities/");
  const REQUEST_HEADER = getRequestHeader(getState().authentication.token);
  Axios.get(PATH, REQUEST_HEADER)
    .then(r => dispatch({ type: GET_LAST_USER_ACTIVITIES, payload: r.data }))
    .catch(e => {
      console.log(e);
      dispatch(
        errorMessage(
          "Falha ao buscar ações do usuário. Verifique sua conexão com a internet."
        )
      );
    })
    .finally(() => dispatch({ type: UNSET_LOADING }));
};

export const getPlans = () => dispatch => {
  dispatch({
    type: SET_LOADING
  });
  const PATH = getPathToApi("available-plans/");
  Axios.get(PATH, REQUEST_HEADER)
    .then(r => dispatch({ type: GET_PLANS, payload: r.data }))
    .catch(e => {
      dispatch({ type: GET_PLANS_FAILED });
      console.log(e);
      dispatch(
        errorMessage(
          "Não foi possível recuperar lista com os planos disponívels."
        )
      );
    })
    .finally(() => dispatch({ type: UNSET_LOADING }));
};

export const getProfiles = () => (dispatch, getState) => {
  dispatch({
    type: SET_LOADING
  });
  const PATH = getPathToApi("profiles/");
  const REQUEST_HEADER = getRequestHeader(getState().authentication.token);

  Axios.get(PATH, REQUEST_HEADER)
    .then(r => dispatch({ type: GET_PROFILES, payload: r.data }))
    .catch(e => {
      dispatch({
        type: GET_PROFILES_FAILED
      });
      console.log(e);
      dispatch(errorMessage("Falha ao buscar por perfis de usuário."));
    })
    .finally(() => dispatch({ type: UNSET_LOADING }));
};

export const authenticateGoogleUser = tokenId => (dispatch, getState) => {
  dispatch({
    type: SET_LOADING
  });
  const REQUEST_HEADER = getRequestHeader(getState().authentication.token);
  const PATH = getPathToApi("validate-google-token/");
  const body = JSON.stringify({ token: tokenId });
  Axios.post(PATH, body, REQUEST_HEADER)
    .then(r =>
      dispatch({
        type: LOGGED_IN_USER,
        payload: r.data
      })
    )
    .catch(err => {
      if (err.response && err.response.status === 403) {
        dispatch(
          errorMessage(
            "Usuário não encontrado ou conta não associada a um usuário 3cIT."
          )
        );
      } else {
        console.error(err);
      }
      dispatch({
        type: AUTHENTICATION_FAILED
      });
    })
    .finally(() => dispatch({ type: UNSET_LOADING }));
};
